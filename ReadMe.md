# metaDedi-Filters

Help us to add more providers to metaDedi.pw

Filters are written in PHP, you can find examples in: examples/

Basically a filter a class, which is loaded dynamically if needed and gets the html code or payload data from the json api.<br />
When everything is parsed, it returns an multidimensional array.

If you like to use regex for that there are some tools you can use:<br />
https://regex101.com/<br />
https://www.phpliveregex.com/

You can also choose a html parser if you like.

To get started, clone this repo, make sure no one else is already working on the filter.<br />
if you see no open issue related to the company, then go ahead, but open an issue so everyone knows someone is already working on it.<br />
Code the filter, submit a merge request.<br />

If you have any questions feel free to ask me.

Explanation of variables used:

| Name          | Info                                              |  Mandatory | Example         |
| :------------ | :-----------------------------------------------: | :--------: | --------------: |
| cpu           | Contains the cpu name from this offer             | Yes        | Atom N2800      |
| memory        | Contains the amount of memory from this offer     | Yes        | 1 or 4GB        |
| storage       | Contains the amount of storage from this offer    | Yes        | 1 or 4TB        |
| traffic       | Contains the amount of traffic from this offer    | Yes        | 1 or 5TB        |
| network       | Contains the speed of network from this offer     | Yes        | 100 or 1000Mbit |
| priceMonthly  | Contains the monthly price                        | Yes        | 4.99 or 29.95   |
| priceYearly   | Contains the monthly price but prepaid yearly     | Yes        | 4.99 or 29.95   |
| orderURL      | Contains the direct order url                     | No         | https://.....   |
| flag          | Override the server location                      | No         | Finland         |
| ssd           | Set flag if ssd is available                      | No         | 1               |

Sites that needed to be added, you can pick one or choose a site thats not listed here by yourself:

https://www.ikoula.com/en/dedicated-server, currently we cannot display the 3 month discount, so just use the normal price.<br />
https://www.firstheberg.com/en/dedicated-server<br />
https://gthost.com/instant-servers/<br />
https://zare.com/dedicated-servers, still waiting for the API access but not sure that this will happen.<br />
