<?php

  class Hetzner {

    public function Filter($text) {
      $json = json_decode($text,true);
      $response = array();
      foreach ($json['server'] as $element) {
        if (empty($element['cpu']) == false AND $element['hdd_count'] > 0) {
          $response[$element['key']]['cpu'] = $element['cpu'];
          $response[$element['key']]['memory'] = $element['ram'];
          $response[$element['key']]['storage'] = round($element['hdd_size'] * $element['hdd_count'] / 1024,1);
          $response[$element['key']]['traffic'] = $element['traffic'];
          $response[$element['key']]['network'] = $element['bandwith'];
          $response[$element['key']]['priceMonthly'] = $element['price'];
          $response[$element['key']]['priceYearly'] = $element['price'];
          if (isset($element['link'])) {
            $response[$element['key']]['orderURL'] = 'https://www.hetzner.com/'.$element['link'];
          } else {
            $response[$element['key']]['orderURL'] = 'https://robot.your-server.de/order/marketConfirm/'.$element['key'].'/culture/en_GB/country/OTHER';
          }
          if (count($element['datacenter']) > 0 && strpos($element['datacenter'][0], 'HEL') !== false) {
            $response[$element['key']]['flag'] = "Finland";
          }
          foreach ($element['specials'] as $special) {
            if ($special == "SSD") {
              $response[$element['key']]['ssd'] = 1;
            }
          }
        }
      }
      return $response;
    }
  }

 ?>
